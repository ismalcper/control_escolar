Vue.component('Ayudageneral',{
    template:`
       <div class="container" v-if="this.mostrar==true">
           <div>
             <h2 style="text-align: center; margin-top: 21px;">Sección de Ayuda</h2>
             <audio src="audio/audio.mp3" controls="controls" type="audio/mpeg" preload="preload"></audio>
           </div>
           <div style="font-size: 20px">
               <ol>
                 <li>¿Que puedo encontrar en la página web?</li>
                 La página cuenta con secciones para ayudar a Profesores y Padres de familia con la navegación en la página de Control Escolar.
                 <br>
                 <li>¿Como puede navegar en la página?</li>
                 La página cuenta con una barra de navegación de fácil uso.
                 <img style="  max-width: 72%;" src="img/general/barra.png"> <br>
                 Cada una con una parte para Profesores y otro para Padres de familia<br>
                 <img style="  max-width: 72%;" src="img/general/opcionesBarra.png"> <br>
                 Y una barra lateral con las partes de cada sección.<br>
                 <img style="  max-width: 72%;" src="img/general/lateral.png"> <br>
                 <br>
                 <li>¿Que puedo encontrar en cada sección?</li>
                  <h5>La sección de Padres de familia cuenta con ayuda para:</h5>
                  &nbsp;&nbsp;Inicio-Padres
                  <ul>
                     <li>Inicio de sesión</li>
                     <li>Datos Solicitados</li>
                     <li>Recuperación de contraseña</li>
                  </ul>
                  &nbsp;&nbsp;Registro-Profesores
                  <ul>
                     <li>Registro Padres</li>
                     <li>Datos Solicitados</li>
                  </ul>
                  <h5>La sección de Profesores cuenta con ayuda para:</h5>
                  &nbsp;&nbsp;Inicio-Profesores
                  <ul>
                     <li>Inicio de sesión</li>
                     <li>Contraseña</li>
                     <li>Recuperación de contraseña</li>
                  </ul>
                  &nbsp;&nbsp;Registr-Profesores
                  &nbsp;&nbsp;
                  <ul>
                     <li>Registro Calificaciones</li>
                     <li>Imprimir lista de Calificaciones</li>
                  </ul>
                 <br>
                 <br>
                 <li>¿Existe alguna otra guia para iniciar sesión?</li>
                     Al ingresar la página web se muestra un pequeño video como muestra para entrar al Sistema de Control Escolar
                     <img style="  max-width: 72%;" src="img/general/img_ayuda.png"> <br>
               </ol>
               <div style="text-align:center;">
                    <h4>Contacto</h4>
                    Clemente Claudio Flores<br>
                    Correo: claudio.flores.clemente@gmail.com<br>
                    Ismael Alcalá<br>
                    Correo:---------------------------------<br>
               </div>
               <br>
               <br>
               <br>
               <br>
               <br>
               <br>
           </div>
       </div>
    `,
    data:function(){
      return{
        mostrar:false
      }
    },
    mounted(){
      Event.listen('MostarAyudaGeneral', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
      });
      Event.listen('OcultarContenidoAyudaGeneral', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });

    }
  })
