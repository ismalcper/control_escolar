///////////////////////////INICIA PARTE DE REGISTRO PADRES/////////////////////////////
Vue.component('Registropadres', {
    template: `
    <div v-if="this.mostrar==true" class="content">
        <div>
          <h2>Registro nuevo</h2>
        </div>
        <div style="font-size: 20px">
            <ol>
              <li>Ingresar al sitio web en:  <a href="https://controlescolar.uaemex.mx/dce/sicde/publico/padres/indexLogin.html"> Control escolar para padres </a></li>
              <img src="img/padres/Registro/Registro/paginaPrincipal.png" alt="">
              <br>
              <li>Click en el botón “Registrarse”.</li>
              <img src="img/padres/Registro/Registro/btnRegistratme.png" alt="">
              <br>
              <li>Llenar el formulario solicitado.</li>
              <img src="img/padres/Registro/Registro/Formulario.png" alt="">
              <br>
              <li>Click en el botón Gurdar.</li>
              <img src="img/padres/Registro/Registro/btnGuardar.png" alt="">
              <br>
              <li>	Iniciar sesión con los datos de registro anterior </li>
             <br>
            </ol>
        </div>
    </div>
    `,
    data: function () {
      return {
        mostrar: false
      }
    },

    mounted() {
      Event.listen('MostrarDatosPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarRegistroPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
      });
      Event.listen('OcultarContenidoRegistroPadres', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });

    }
  })

  Vue.component('Datospadres', {
    template: `
    <div v-if="this.mostrar==true" class="content">
        <div>
          <h2>Datos Solicitados</h2>
        </div>
        <div style="font-size: 20px">
            <ol>
              <li>Usuario. Escribe tu nombre o una frase que de identifique.</li>
              <img src="img/padres/Registro/DatosSolicitados/Usuario.png" alt="">
              <br>
              <br>
              <li>Contraseña. Escribe una frase la que será clave de acceso.</li>
              <img src="img/padres/Registro/DatosSolicitados/Contra.png" alt="">
              <br>
              <li>Nombre. Escribe el nombre completo del padre o tutor.</li>
              <img src="img/padres/Registro/DatosSolicitados/Nombre.png" alt="">
              <br>
              <li>Correo electrónico. Escribe un correo electrónico válido, este te servirá
              para recuperar tu contraseña en caso de que la olvides.</li>
              <img src="img/padres/Registro/DatosSolicitados/Correo.png" alt="">
              <br>
            </ol>
        </div>
    </div>
    `,
    data: function () {
      return {
        mostrar: false
      }
    },
    mounted() {
      Event.listen('MostrarRegistroPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarDatosPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
      });
      Event.listen('OcultarContenidoRegistroPadres', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
    }
  })

  Vue.component('barraregistropadres', {
    template: `
     <div v-if="this.mostrar==true">
            <img class="indicador" src="img/esqPa.png" alt="">
        <div class="sidebar">
            <a class="active" href="#">Contenido</a>
                    <a v-on:click="RegistroPadres()">Registro</a>
                    <a v-on:click="DatosSolicitadosPadres()">Datos Solicitados</a>

        </div>
     </div>
     `,
    data: function () {
      return {
        mostrar: false
      }
    },
    mounted() {
      Event.listen('OcultarContenidoRegistroPadres', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
    Event.listen('MostrarBarraRegistroPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
        Event.fire('MostrarDatosPadres');
    });

    },
    methods: {
      /*Funciones que realizaran los botones de la parte template*/
      DatosSolicitadosPadres() {
        myFunction();
        Event.fire('MostrarDatosPadres');
      },
      RegistroPadres() {
        myFunction();
        Event.fire('MostrarRegistroPadres');
      },
    }
  })
  ///------------------------- TERMINA PARTE DE REGISTRO PADRES-----------------------------
