///////////////////////////INICIA PARTE DE INICIO PROFESORES/////////////////////////////
Vue.component('Contraprofesores', {
    template: `
    <div v-if="this.mostrar==true" class="content">
        <div>
          <h2>Requisitos de la Contraseña</h2>
        </div>
        <div style="float: right; ">
          <img src="img/Ejem.png" style="width:90%; height:90%;">
        </div>
        <div style="font-size: 20px">
            <ol>
            <li>Al menos una letra mayúscula.</li>
            <br>
            <li>Al menos una letra minúscula.</li>
            <br>
            <li>Un caracter especial (#?!@$%^&*-.).</li>
            <br>
            <li>Un número.</li>
            <br>
            <li>Debe de tener un tamaño mínimo de 8 caracteres y un tamaño máximo de 20.</li>
            <br>
            <li>No debe de contener espacios en blanco.</li>
            </ol>
        </div>
    </div>
    `,
    data: function () {
      return {
        mostrar: false
      }
    },
    mounted() {
      Event.listen('MostrarContraProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true como default
        this.mostrar = true;
      });
      Event.listen('MostrarOpcionesProfesores', () => {
        // se confirma que el contenido no se esta mostrando
        this.mostrar = false;
      });
      Event.listen('MostrarRecuperaProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarUsuarioProfesores', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
      Event.listen('OcultarContenidoInicioProfesores', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
    }
  })
  
  Vue.component('Recuperacionprofesores', {
    template: `
    <div v-if="this.mostrar==true" class="content">
  
        <div>
           <h2>Recuperación de Contraseña</h2>
        </div>
        <div style="font-size: 20px">
            <ol>
                <li>Ingresar a las pagina de control escolar colocando en nuestro navegador el siguiente link: <a href="https://controlescolar.uaemex.mx">https://controlescolar.uaemex.mx</a>.</li>
                <br>
                <li>Dar click en el apartado "Profesores (calificaciones)".</li>
                <br>
                <li>Dar click en "He leído la información"</li>
                <img src="img/2.png" alt="">
                <br>
                <li>Seleccionar el espacio academico donde actualmente labora</li>
                <img src="img/3.png" alt="">
                <br>
                <li>Dar click en olvide mi contraseña</li>
                <img src="img/4.png" alt="">
                <br>
                <li>Llenar el recuadro con su correo correspondiente</li>
                <img src="img/5.png" alt="">
                <br>
                <li>Click en enviar.</li>
                <li>Revisa tu correo</li>
                <br>
            </ol>
        </div>
    </div>
    `,
    data: function () {
      return {
        mostrar: false
      }
    },
    mounted() {
      Event.listen('MostrarContraProfesores', () => {
        //se confirma que el contenido no se esta mostrando
        this.mostrar = false;
      });
      Event.listen('MostrarOpcionesProfesores', () => {
        // se confirma que el contenido no se esta mostrando
        this.mostrar = false;
      });
      Event.listen('MostrarUsuarioProfesores', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
      Event.listen('MostrarRecuperaProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
      });
      Event.listen('OcultarContenidoInicioProfesores', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
    }
  })
  
  
  
  Vue.component('Usuarioprofesores', {
    template: `
    <div v-if="this.mostrar==true" class="content">
     <h1>Inicio de sesión</h1>
            <div style="float: right;">
            <video width="400" controls preload="none" height="250" src="video/videoLogin.ogv" frameborder="0" allowfullscreen></video>
            </div>
        <div style="font-size: 20px">
            <ol>
                <li>Ingresar a la pagina de <a href="https://controlescolar.uaemex.mx" target="_blank">Control Escolar</a>.
                </li>
                <br>
                <li>Dar click en el apartado "Profesores (calificaciones)".</li>
                <br>
                <li>Dar click en "He leído la información"</li>
                <img src="img/2.png" alt="">
                <br>
                <li>Seleccionar el espacio academico donde actualmente labora</li>
                <img src="img/3.png" alt="">
                <br>
                <li>Llenar los campos requeridos y dar click en Firmarse</li>
                <img src="img/4.1.png" alt="">
            </ol>
        </div>
    </div>
    `,
    data: function () {
      return {
        mostrar: false
      }
    },
    mounted() {
      Event.listen('MostrarContraProfesores', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
      Event.listen('MostrarRecuperaProfesores', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
      Event.listen('MostrarOpcionesProfesores', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
      Event.listen('MostrarUsuarioProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
      });
      Event.listen('OcultarContenidoInicioProfesores', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
    }
  })
  
  Vue.component('Barrainicioprofesores', {
    template: `
    <div  v-if="this.mostrar==true">
           <img class="indicador" src="img/esqP.png" alt="">
        <div class="sidebar">
            <a class="active" href="#">Contenido</a>
                     <a v-on:click="UsuarioProfesores()">Inicio</a>
                     <a v-on:click="ContraseñaProfesores()">Contraseña</a>
                     <a v-on:click="RecuparaProfesores()">Recuperación de contraseña</a>
        </div>
     </div>
     `,
    data: function () {
      return {
        mostrar: false
      }
    },
    mounted() {
      Event.listen('MostrarBarraInicioPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarBarraInicioProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
      });
      Event.listen('MostrarBarraRegistroPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarBarraRegistroProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
    },
    methods: {
      /*Funciones que realizaran los botones de la parte template*/
      ContraseñaProfesores() {
        // se emite un evneto, el valor se manda en 1 uno para mostrar el Contenido
        // del componente Contra
        myFunction();
        Event.fire('MostrarContraProfesores');
      },
      RecuparaProfesores() {
        // se emite un evneto, el valor se manda en 1 uno para mostrar el Contenido
        // del componente Contra
        myFunction();
        Event.fire('MostrarRecuperaProfesores');
      },
      OpcionesProfesores() {
        myFunction();
        Event.fire('MostrarOpcionesProfesores');
      },
      UsuarioProfesores() {
        myFunction();
        Event.fire('MostrarUsuarioProfesores');
      },
    }
  })
  ///------------------------- TERMINA PARTE DE INCIO PROFESORES-----------------------------
  