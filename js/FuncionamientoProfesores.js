
Vue.component('Herramientaprofesores', {
  template: `
  <div v-if="this.mostrar==true" class="content">
      <div style="font-size: 20px">
          <ol>
              <h4>Considerando que ha iniciado sesión en el sistema.</h4>
              <li>Click en Herramientas</li>
              En caso de querer ver el correo con el que sé está registrado dar click Correo personal registro. <br>
              En caso de querer ver el correo institucional dar click Cuenta de correo institucional. <br>
              <img src="img/6.png" style="height:30%;width:30%;">
          </ol>
      </div>
  </div>
  `,
  data: function () {
    return {
      mostrar: true
    }
  },
  mounted() {
    Event.listen('OcultarContenidoFuncionamientoProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = false;
    });
    Event.listen('MostrarDatosGeneralesProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = false;
    });
    Event.listen('MostrarCursosProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = false;
    });
    Event.listen('MostrarHerramientasProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = true;
    });
  }
})

Vue.component('Cursoprofesores', {
  template: `
  <div v-if="this.mostrar==true" class="content">
      <div style="font-size: 20px">
          <ol>
              <h4>Considerando que ha iniciado sesión en el sistema.</h4>
              <li>Click en cursos impartidos</li>
              La sección cuenta con una lista de los alumnos de los cursos que a importido o esta impartiendo
              <br>
              <img src="img/6.png" style="height:30%;width:30%;">
          </ol>
      </div>
  </div>
  `,
  data: function () {
    return {
      mostrar: true
    }
  },
  mounted() {
    Event.listen('OcultarContenidoFuncionamientoProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = false;
    });
    Event.listen('MostrarDatosGeneralesProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = false;
    });
    Event.listen('MostrarCursosProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = true;
    });
    Event.listen('MostrarHerramientasProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = false;
    });
  }
})

Vue.component('Datosgeneralprofesores', {
  template: `
  <div v-if="this.mostrar==true" class="content">
      <div style="font-size: 20px">
          <ol>
              <h4>Considerando que ha iniciado sesión en el sistema.</h4>
              <li>Click en Datos Generales</li><br>
              En este apartado se pueden encontrar datos como Nombre, Apellido Materno, Apellido Materno Correo electrónico del profesor que este utilizando el sistema.
              <img src="img/6.png" style="height:30%;width:30%;">
          </ol>
      </div>
  </div>
  `,
  data: function () {
    return {
      mostrar: true
    }
  },
  mounted() {
    Event.listen('OcultarContenidoFuncionamientoProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = false;
    });
    Event.listen('MostrarDatosGeneralesProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = true;
    });
    Event.listen('MostrarCursosProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = false;
    });
    Event.listen('MostrarHerramientasProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = false;
    });
  }
})

Vue.component('Barrafuncionprofesores', {
  template: `
  <div  v-if="this.mostrar==true">
         <img class="indicador" src="img/esqP.png" alt="">
      <div class="sidebar">
          <a class="active" href="#">Contenido</a>
                   <a v-on:click="Datos_generales()">Datos Generales</a>
                   <a v-on:click="Cursos()">Cursos Impartidos</a>
                   <a v-on:click="Herramientas()">Herramientas</a>
      </div>
   </div>
   `,
  data: function () {
    return {
      mostrar: false
    }
  },
  mounted() {
    Event.listen('MostrarBarraFuncProfesores', () => {
      this.mostrar = true;
      Event.fire('MostrarDatosGeneralesProfesores');
    });
    Event.listen('OcultarContenidoFuncionamientoProfesores', () => {
      // se confirma que no debe mostrar este contenido
      this.mostrar = false;
    });
  },
  methods: {
    Datos_generales(){
      Event.fire('MostrarDatosGeneralesProfesores');
    },
    Cursos(){
      Event.fire('MostrarCursosProfesores');
    },
    Herramientas(){
      Event.fire('MostrarHerramientasProfesores');
    }
  }
})
