///////////////////////////INICIA PARTE DE REGISTRO PROFESORES/////////////////////////////
Vue.component('Registrocalificacion', {
    template: `
    <div v-if="this.mostrar==true" class="content">
        <div>
          <h2>Registro de calificaciones</h2>
        </div>
        <div style="font-size: 20px">
            <ol>
              <li>Una vez iniciada sesión.</li>
              <br>
              <li>Seleccionar cursos impartidos.</li>
              <br>
              <img src="img/7.png" alt="">
              <br>
              <li>Se mostraran los alumnos incritos al curso.</li>
              <br>
              <img src="img/7.png" alt="">
              <br>
              <li>Seleccionar calificaciones.</li>
              <br>
              <img src="img/8.png" alt="">
              <br>
              <li>Seleccione editar(Por favor tome en cuenta que esta opción solo está disponible en las fechas marcadas en el calendario de exámenes).</li>
              <br>
              <img src="img/9.png" alt="">
              <br>
            </ol>
        </div>
    </div>
    `,
    data: function () {
      return {
        mostrar: false
      }
    },
    mounted() {
      Event.listen('OcultarContenidoRegistroProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarRegistroCalificaciones', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
      });
      Event.listen('Imprimelista', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
    }
  })
  Vue.component('Imprimirlistacal', {
    template: `
    <div v-if="this.mostrar==true" class="content">
        <div>
          <h2>Imprimir lista de calificaciones</h2>
        </div>
        <div style="font-size: 20px">
            <ol>
              <li>Una vez iniciada sesión.</li>
              <br>
              <li>Seleccionar cursos impartidos.</li>
              <br>
              <img src="img/7.png" alt="">
              <br>
              <li>Se mostraran los alumnos incritos al curso.</li>
              <br>
              <img src="img/7.png" alt="">
              <br>
              <li>Seleccionar calificaciones.</li>
              <br>
              <img src="img/8.png" alt="">
              <br>
              <li>Seleccione Imprimir (Por favor tome en cuenta que esta opción solo está disponible despues de haber registrado las calificaciones).</li>
              <br>
              <img src="img/10.png" alt="">
              <br>
            </ol>
        </div>
    </div>
    `,
    data: function () {
      return {
        mostrar: false
      }
    },
    mounted() {
      Event.listen('OcultarContenidoRegistroProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('Imprimelista', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
      });
      Event.listen('MostrarRegistroCalificaciones', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
    }
  })
  Vue.component('Barraregistroprofesores', {
    template: `
     <div v-if="this.mostrar==true">
        
            <img class="indicador" src="img/esqP.png" alt="">
         <div class="sidebar">
  
               <a class="active" href="#">Contenido</a>
  
                     <a v-on:click="RegistroCalificaciones()">Registro de calificaciones</a>
                     <a v-on:click="ImprimirLista()">Imprimir lista de calificaciones</a>
  
         </div>
     </div>
     `,
    data: function () {
      return {
        mostrar: false
      }
    },
    mounted() {
      Event.listen('MostrarBarraInicioPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarBarraInicioProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarBarraRegistroPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarBarraRegistroProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
        Event.fire('MostrarRegistroCalificaciones');
      });
      Event.listen('OcultarContenidoRegistroProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
    },
    methods: {
      /*Funciones que realizaran los botones de la parte template*/
      RegistroCalificaciones() {
        myFunction();
        Event.fire('MostrarRegistroCalificaciones');
      },
      ImprimirLista() {
        myFunction();
        Event.fire('Imprimelista');
      }
    }
  })
  //------------------------- TERMINA PARTE DE REGISTRO PROFESORES-----------------------------
  