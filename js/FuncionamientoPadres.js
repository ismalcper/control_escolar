Vue.component('Misdependientes', {
    template: `
    <div v-if="this.mostrar==true" class="content">
        <div>
          <h2>Mis dependientes</h2>
        </div>
        <div style="font-size: 20px">
        <video width="500" controls preload="none" height="350" src="video/misDependientes.mp4" frameborder="0" allowfullscreen></video>
           
        </div>
    </div>
    `,
    data: function () {
        return {
            mostrar: false
        }
    },

    mounted() {
        Event.listen('MostrarTrayectorias', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarAnalisis', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarCorreoFuncionamientoPadres', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarCambioContra', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('OcultarContenidoFuncionamientoPadres', () => {
            // se confirma que no debe mostrar este contenido
            this.mostrar = false;
        });
        Event.listen('MostrarMisDependientes', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = true;
        });

    }
})

Vue.component('Trayectorias', {
    template: `
    <div v-if="this.mostrar==true" class="content">
        <div>
          <h2>Trayectoria</h2>
        </div>
        <div style="font-size: 20px">
         <ol>
       
            <li>Dar click en el apartado "Trayectorias".</li>
            <img src="img/padres/Funcionamiento/Trayectoria/Captura.png" alt="">
            <br>
            <li>Dar click en el número de cuenta de nuestro dependiente.</li>
            <img src="img/padres/Funcionamiento/Trayectoria/cuenta.png" alt="">
            <br>
            <li>Apartado clificaciones.</li>
            <img src="img/padres/Funcionamiento/Trayectoria/calificaciones.png" alt="">
            <br>
            <li>Apartado indicadores.</li>
            <img src="img/padres/Funcionamiento/Trayectoria/indicadores.png" alt="">
            <br>
            <li>Apartado Análisis.</li>
            <img src="img/padres/Funcionamiento/Trayectoria/analisis.png" alt="">
            <br>
            <li>Apartado Currícula.</li>
             <img src="img/padres/Funcionamiento/Trayectoria/curricula.png" alt="">
            <br>
            <li>Apartado Líneas de estudio.</li>
            <img src="img/padres/Funcionamiento/Trayectoria/linea.png" alt="">
         </ol>
        </div>
    </div>
    `,
    data: function () {
        return {
            mostrar: false
        }
    },
    mounted() {
        Event.listen('MostrarMisDependientes', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarAnalisis', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarCorreoFuncionamientoPadres', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarCambioContra', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('OcultarContenidoFuncionamientoPadres', () => {
            // se confirma que no debe mostrar este contenido
            this.mostrar = false;
        });
        Event.listen('MostrarTrayectorias', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = true;
        });

    }
})

Vue.component('Analisis', {
    template: `
    <div v-if="this.mostrar==true" class="content">
        <div>
          <h2>Análisis por ciclo</h2>
        </div>
        <div style="font-size: 20px">
        <ol>
       
        <li>Dar click en el apartado "Análisis por ciclo".</li>
        <img src="img/padres/Funcionamiento/Analisis/Captura.png" alt="">
        <br>
        <li>Seleccionar un plan de estudios.</li>
        <img src="img/padres/Funcionamiento/Analisis/selector.png" alt="">
        <br>
        <li>Seleccionar un periodo.</li>
        <img src="img/padres/Funcionamiento/Analisis/analisis.png" alt="">
        <br>
        <li>Cursos.</li>
        <img src="img/padres/Funcionamiento/Analisis/periodo.png" alt="">
        <br>
        <li>Calificaciones.</li>
        <img src="img/padres/Funcionamiento/Analisis/calificaciones.png" alt="">
        
        
     </ol>
        </div>
    </div>
    `,
    data: function () {
        return {
            mostrar: false
        }
    },
    mounted() {
        Event.listen('MostrarMisDependientes', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarTrayectorias', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarCorreoFuncionamientoPadres', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarCambioContra', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('OcultarContenidoFuncionamientoPadres', () => {
            // se confirma que no debe mostrar este contenido
            this.mostrar = false;
        });
        Event.listen('MostrarAnalisis', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = true;
        });

    }
})

Vue.component('Correopersonal', {
    template: `
    <div v-if="this.mostrar==true" class="content">
        <div>
          <h2>Correo personal registrado</h2>
        </div>
        <div style="font-size: 20px">
        <ol>
       
        <li>Dar click en el apartado "Correo personal registrado".</li>
        <img src="img/padres/Funcionamiento/Correo/Captura.png" alt="">
        
        
     </ol>
        </div>
    </div>
    `,
    data: function () {
        return {
            mostrar: false
        }
    },
    mounted() {
        Event.listen('MostrarMisDependientes', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarTrayectorias', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarAnalisis', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarCambioContra', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('OcultarContenidoFuncionamientoPadres', () => {
            // se confirma que no debe mostrar este contenido
            this.mostrar = false;
        });
        Event.listen('MostrarCorreoFuncionamientoPadres', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = true;
        });


    }
})

Vue.component('Cambiocontrasena', {
    template: `
    <div v-if="this.mostrar==true" class="content">
        <div>
          <h2>Cambio de contraseña</h2>
        </div>
        <div style="font-size: 20px">
        <ol>
       
        <li>Dar click en el apartado "Cambio de contraseña".</li>
        <img src="img/padres/Funcionamiento/contraseña/Captura.png" alt="">
        <br>
        <li>Llenar el formulario con la contraseña actual y una nueva.</li>
        <img src="img/padres/Funcionamiento/contraseña/nuevaForm.png" alt="">
        <br>
        <li>Dar click en el botón "Guardar".</li>
        <img src="img/padres/Funcionamiento/contraseña/guardar.png" alt="">
       
     </ol>
        </div>
    </div>
    `,
    data: function () {
        return {
            mostrar: false
        }
    },
    mounted() {
        Event.listen('MostrarMisDependientes', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarTrayectorias', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarAnalisis', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('MostrarCorreoFuncionamientoPadres', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = false;
        });
        Event.listen('OcultarContenidoFuncionamientoPadres', () => {
            // se confirma que no debe mostrar este contenido
            this.mostrar = false;
        });
        Event.listen('MostrarCambioContra', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = true;
        });

    }
})

Vue.component('barrafunpadres', {
    template: `
     <div v-if="this.mostrar==true">
            <img class="indicador" src="img/esqPa.png" alt="">
        <div class="sidebar">
            <a class="active" href="#">Contenido</a>
                    <a v-on:click="MisDependientes()">Mis dependientes</a>
                    <a v-on:click="Trayectorias()">Trayectorias</a>
                    <a v-on:click="Analisis()">Análisis por ciclo</a> 
                    <a v-on:click="Correo()">Correo personal</a> 
                    <a v-on:click="CambioContrasena()">Cambio de contraseña</a>            
                  
        </div>
     </div>
     `,
    data: function () {
        return {
            mostrar: false
        }
    },
    mounted() {

        Event.listen('OcultarContenidoFuncionamientoPadres', () => {
            // se confirma que no debe mostrar este contenido
            this.mostrar = false;
        });
        Event.listen('MostrarBarraFuncPadres', () => {
            // se emitio el evento correspondiente por eso se pone true
            this.mostrar = true;
            Event.fire('MostrarMisDependientes');
        });
    },
    methods: {
        /*Funciones que realizaran los botones de la parte template*/
        MisDependientes() {
            myFunction();
            Event.fire('MostrarMisDependientes');
        },
        Trayectorias() {
            myFunction();
            Event.fire('MostrarTrayectorias');
        },
        Analisis() {
            myFunction();
            Event.fire('MostrarAnalisis');
        },
        Correo() {
            myFunction();
            Event.fire('MostrarCorreoFuncionamientoPadres');
        },
        CambioContrasena() {
            myFunction();
            Event.fire('MostrarCambioContra');
        },
    }
})
///------------------------- TERMINA PARTE DE REGISTRO PADRES-----------------------------