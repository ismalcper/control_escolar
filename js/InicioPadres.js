Vue.component('Contra', {
    template: `
    <div v-if="this.mostrar==true" class="content">
    <h1>Datos solicitados</h1>
    <ol>
        <li>Usuario: Escribe tu nombre de usuario con el que te registraste (<a>Registro</a>)</li>
        <img src="img/padres/Inicio/datos solicitados/DatosSoliUsuario.png" alt="">
        <li>Contraseña: Escribe la contraseña con la cual registraste el usuario (<a>Registro</a>)</li>
        <img src="img/padres/Inicio/datos solicitados/DatosSoliContra.png" alt="">
    </ol>
    </div>
    `,
    data: function () {
      return {
        mostrar: false
      }
    },
    mounted() {
      Event.listen('MostrarContra', () => {
        // se emitio el evento correspondiente por eso se pone true como default
        this.mostrar = true;
      });
      Event.listen('MostrarOpciones', () => {
        // se confirma que el contenido no se esta mostrando
        this.mostrar = false;
      });
      Event.listen('MostrarRecupera', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarUsuario', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
      Event.listen('OcultarContenidoInicioPadres', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
    }
  })
  
  Vue.component('Recuperacion', {
    template: `
    <div v-if="this.mostrar==true"  class="content">
  
    <h1>Recuperacion de contraseña</h1>
    <ol>
        <li>Ingresar al sitio web (explicado en el apartado “<a>inicio</a>”).</li>
        <li>Click en olvidé mi contraseña.</li>
        <img src="img/padres/Inicio/Recuperacion de contrasena/OlvideContra.png" alt="">
        <li>Ingresar nuestro correo electronico registrado</li>
        <img src="img/padres/Inicio/Recuperacion de contrasena/correoElectro.png" alt="">
        <li>Click en "Enviar"</li>
        <img src="img/padres/Inicio/Recuperacion de contrasena/BotonEnviar.png" alt="">
        <li>Dar click en el boton "Aceptar"</li>
        <img src="img/padres/Inicio/Recuperacion de contrasena/BotonAceptar.png" alt="">
        <li>Revisar nuestro correo electronico en donde encontraremos nuestro usuarion y contraseña.</li>
        <img src="img/padres/Inicio/Recuperacion de contrasena/RevisarCorreo.png" alt="">
        <img src="img/padres/Inicio/Recuperacion de contrasena/DatosCorreo.png" alt="">
    </ol>
    </div>
    `,
    data: function () {
      return {
        mostrar: false
      }
    },
    mounted() {
      Event.listen('MostrarContra', () => {
        //se confirma que el contenido no se esta mostrando
        this.mostrar = false;
      });
      Event.listen('MostrarOpciones', () => {
        // se confirma que el contenido no se esta mostrando
        this.mostrar = false;
      });
      Event.listen('MostrarUsuario', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
      Event.listen('MostrarRecupera', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
      });
      Event.listen('OcultarContenidoInicioPadres', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
    }
  })
  
  
  
  Vue.component('Usuario', {
    template: `
    <div v-if="this.mostrar==true" class="content">
            <div style="float: right;">
            <video width="400" controls preload="none" height="250" src="video/videoLogin.ogv" frameborder="0" allowfullscreen></video>
            </div>
        <div style="font-size: 20px">
            <ol>
                <li>Ingresar a las pagina de control escolar colocando en nuestro navegador el siguiente link: <a href="https://controlescolar.uaemex.mx">https://controlescolar.uaemex.mx</a>.</li>
                <br>
                <li>Dar click en el apartado "Padres de familia".</li>
                <br>
                <li>Seleccionar la sede en la cual se encuentra inscrito nuestro dependiente (hijo), en ete ejemplo damos click en "Ingenería".</li>
                <br>
                <li>Colocar el nombre de usuario que elegimos cuando nos registramos, para fines de este ejemplo usaremos "1571925".</li>
                <br>
                <li>Colocar la contraseña que indicamos al momento de realizar el registro.</li>
                <br>
                <li>Dar click en el boton "Firmarse".</li>
            </ol>
        </div>
    </div>
    `,
    data: function () {
      return {
        mostrar: true
      }
    },
    mounted() {
      Event.listen('MostrarContra', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
      Event.listen('MostrarRecupera', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
      Event.listen('MostrarOpciones', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
      Event.listen('MostrarUsuario', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
      });
      Event.listen('OcultarContenidoInicioPadres', () => {
        // se confirma que no debe mostrar este contenido
        this.mostrar = false;
      });
    }
  })
  
  Vue.component('Barrainiciopadres', {
    template: `
    <div  v-if="this.mostrar==true">
    <img class="indicador" src="img/esqPa.png" alt="">
        <div class="sidebar">
                <a class="active" href="#">Contenido</a></li>
                <a v-on:click="Usuario()">Inicio</a>
                <a v-on:click="Contraseña()">Datos solicitados</a>
                <a v-on:click="Recupara()">Recuperación de contraseña</a>
        </div>
  
  
    </div>
     `,
    data: function () {
      return {
        mostrar: true
      }
    },
    mounted() {
      Event.listen('MostrarBarraInicioPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = true;
      });
      Event.listen('MostrarBarraInicioProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarBarraRegistroPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('MostrarBarraRegistroProfesores', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
      Event.listen('OcultarContenidoInicioPadres', () => {
        // se emitio el evento correspondiente por eso se pone true
        this.mostrar = false;
      });
    },
    methods: {
      /*Funciones que realizaran los botones de la parte template*/
      Contraseña() {
        // se emite un evneto, el valor se manda en 1 uno para mostrar el Contenido
        // del componente Contra
        myFunction();
        Event.fire('MostrarContra');
      },
      Recupara() {
        // se emite un evneto, el valor se manda en 1 uno para mostrar el Contenido
        // del componente Contra
        myFunction();
        Event.fire('MostrarRecupera');
      },
      Usuario() {
        myFunction();
        Event.fire('MostrarUsuario');
      },
    }
  })
  ///------------------------- TERMINA PARTE DE INCIO PADRES-----------------------------
  