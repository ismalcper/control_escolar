window.Event = new class {

  constructor() {
    this.vue = new Vue;
  }

  fire(event, data = null) {
    this.vue.$emit(event, data);
    //emit, emite los eventos de los componentes
  }

  listen(event, callback) {
    this.vue.$on(event, callback);
    //on, escucha los eventos de los componentes
  }
}




///////////////////////////INICIA PARTE DE FUNCIONAMIENTO PADRES/////////////////////////////

///------------------------- TERMINA PARTE DE FUNCIONAMIENTO PADRES-----------------------------

///////////////////////////INICIA PARTE DE FUNCIONAMIENTO PROFESORES/////////////////////////////

///------------------------- TERMINA PARTE DE FUNCIONAMIENTO PROFESORES-----------------------------
///////////////////////////INICIA PARTE DE INCIO PADRES/////////////////////////////

var app = new Vue({
  el: '#Opciones',
  data() {
    return {}
  },
  methods: {
    PadresInicio() {
      Event.fire('MostrarBarraInicioPadres'); //Cargar la Barra de Profesores inicio
      Event.fire('MostrarUsuario'); //Cargar el contenido de Profesores inicio
      Event.fire('OcultarContenidoInicioProfesores');
      Event.fire('OcultarContenidoRegistroProfesores');
      Event.fire('OcultarContenidoRegistroPadres');
      Event.fire('OcultarContenidoAyudaGeneral');
      Event.fire('OcultarContenidoFuncionamientoProfesores');
      Event.fire('OcultarContenidoFuncionamientoPadres');
    },
    ProfesoresInicio() {
      Event.fire('MostrarBarraInicioProfesores'); //Cargar la Barra de Profesores inicio
      Event.fire('MostrarUsuarioProfesores'); //Cargar el contenido de Profesores inicio
      Event.fire('OcultarContenidoInicioPadres');
      Event.fire('OcultarContenidoRegistroProfesores');
      Event.fire('OcultarContenidoRegistroPadres');
      Event.fire('OcultarContenidoAyudaGeneral');
      Event.fire('OcultarContenidoFuncionamientoProfesores');
      Event.fire('OcultarContenidoFuncionamientoPadres');
    },
    PadresRegistro() {
      Event.fire('OcultarContenidoInicioProfesores');
      Event.fire('OcultarContenidoInicioPadres');
      Event.fire('MostrarBarraRegistroPadres');
      Event.fire('OcultarContenidoRegistroProfesores');
      Event.fire('OcultarContenidoAyudaGeneral');
      Event.fire('MostrarRegistroPadres');
      Event.fire('OcultarContenidoFuncionamientoProfesores');
      Event.fire('OcultarContenidoFuncionamientoPadres');
    },
    ProfesoresRegistro() {
      Event.fire('MostrarBarraRegistroProfesores');
      Event.fire('OcultarContenidoInicioProfesores');
      Event.fire('OcultarContenidoInicioPadres');
      Event.fire('OcultarContenidoRegistroPadres');
      Event.fire('OcultarContenidoAyudaGeneral');
      Event.fire('OcultarContenidoFuncionamientoProfesores');
      Event.fire('OcultarContenidoFuncionamientoPadres');
    },
    PadresFuncionamiento() {
      Event.fire('OcultarContenidoInicioProfesores');//Oculta el contenido de PROFESORES
      Event.fire('OcultarContenidoInicioPadres');//Oculta el contenido de Padres
      Event.fire('OcultarContenidoRegistroProfesores');// oculta el contenido de Profesores
      Event.fire('OcultarContenidoRegistroPadres');
      Event.fire('OcultarContenidoAyudaGeneral');
      Event.fire('OcultarContenidoFuncionamientoProfesores');
      Event.fire('MostrarBarraFuncPadres');
    },
    ProfesoresFuncionamiento() {
      Event.fire('OcultarContenidoInicioProfesores');//Oculta el contenido de PROFESORES
      Event.fire('OcultarContenidoInicioPadres');//Oculta el contenido de Padres
      Event.fire('OcultarContenidoRegistroProfesores');// oculta el contenido de Profesores
      Event.fire('OcultarContenidoRegistroPadres');
      Event.fire('OcultarContenidoFuncionamientoPadres');
      Event.fire('OcultarContenidoAyudaGeneral');
      Event.fire('MostrarBarraFuncProfesores');
    },
    AyudaGeneral(){
      Event.fire('OcultarContenidoFuncionamientoProfesores');
      Event.fire('OcultarContenidoInicioProfesores');//Oculta el contenido de PROFESORES
      Event.fire('OcultarContenidoInicioPadres');//Oculta el contenido de Padres
      Event.fire('OcultarContenidoRegistroProfesores');// oculta el contenido de Profesores
      Event.fire('OcultarContenidoRegistroPadres');
      Event.fire('OcultarContenidoFuncionamientoPadres');
      Event.fire('MostarAyudaGeneral');
    }
  }
});
